## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Load stories data
```bash
$ npm run load-data
```

## Go to GraphQl playground 

```bash
http://localhost:3000/graphql
```

### List all stories that are “public” and have more than 20 likes.

```
query {
  getLikedAndPublic {
    title,
    launch_date,
    likes,
    privacy
  }
}
```

### Create Story

```
mutation {
  createStory (
    createStoryInput: {
    likes: 10,
    launch_date: "2010-12-07",
    privacy: "public",
    title: "Story2"
    }) {
    title,
    launch_date,
    privacy,
    likes
  }
}
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```