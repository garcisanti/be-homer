import * as fs from 'fs'
import * as csv from 'csv-parser'
import * as mongoose from 'mongoose'

mongoose.connect('mongodb://localhost/homer', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

(function() {
    
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    const StoryModel = mongoose.model('Story', new mongoose.Schema(
        {
            launch_date: Date,
            title: String,
            privacy: String,
            likes: Number
        }
    ));
    db.once('open', async function() {
        console.log('DB connection is Ok!')
        await StoryModel.deleteMany({})
    
        const csvPipe = csv()
    
        const readStream = fs.createReadStream('./data/stories.csv')
    
        readStream.on('error', err => {
            console.log('File not found')
        })
        
        const storiesPromises: Array<Promise<any>> = []
        
        readStream.pipe(csvPipe).on('data', (storyRow: Story) => {
            const story = new StoryModel(storyRow)
            storiesPromises.push(story.save())
        }).on('end', async function() {
            await Promise.all(storiesPromises)
            process.exit()
        })

    });

})()


interface Story {
    launch_date: Date
    title: string
    privacy: string
    likes: number
}