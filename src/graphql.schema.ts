
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class CreateStoryInput {
    launch_date?: Date;
    title?: string;
    privacy?: string;
    likes?: number;
}

export abstract class IMutation {
    abstract createStory(createStoryInput?: CreateStoryInput): Story | Promise<Story>;
}

export abstract class IQuery {
    abstract getStories(): Story[] | Promise<Story[]>;

    abstract getLikedAndPublic(): Story[] | Promise<Story[]>;

    abstract story(title: string): Story | Promise<Story>;
}

export class Story {
    id?: string;
    launch_date?: Date;
    title?: string;
    privacy?: string;
    likes?: number;
}

export abstract class ISubscription {
    abstract storyCreated(): Story | Promise<Story>;
}
