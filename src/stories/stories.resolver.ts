import { ParseIntPipe, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';
import { Story } from '../graphql.schema';
import { CatsGuard } from './stories.guard';
import { StoriesService } from './stories.service';
import { CreateStoryDto } from './dto/create-story.dto';

const pubSub = new PubSub();

@Resolver('Story')
export class StoriesResolvers {
  constructor(private readonly storiesService: StoriesService) {}

  //   @UseGuards(CatsGuard)
  @Query()
  async getStories() {
    return this.storiesService.findAll();
  }

  @Query()
  async getLikedAndPublic() {
    return this.storiesService.findLikedAndPublic()
  }

  @Query('story')
  async findOneByTitle(
    @Args('title')
    title: string,
  ): Promise<Story> {
    return this.storiesService.findOneByTitle(title);
  }


  @Mutation('createStory')
  async create(@Args('createStoryInput') args: CreateStoryDto): Promise<Story> {
    const createdStory = await this.storiesService.create(args);
    pubSub.publish('storyCreated', { storyCreated: createdStory });
    return createdStory;
  }

  @Subscription('storyCreated')
  storyCreated() {
    return pubSub.asyncIterator('storyCreated');
  }
}