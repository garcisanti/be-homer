import { Module } from '@nestjs/common';
import { StoriesResolvers } from './stories.resolver';
import { StoriesService } from './stories.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StorySchema } from './schema/story. schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Story', schema: StorySchema }])
  ],
  providers: [StoriesService, StoriesResolvers],
})
export class StoriesModule {}