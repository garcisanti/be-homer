
import { Injectable } from '@nestjs/common';
import { Story } from '../graphql.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { StoryModel } from './models/story.model';

@Injectable()
export class StoriesService {

  constructor(@InjectModel('Story') private readonly storyModel: Model<StoryModel>) {}

  create(story: Story): Promise<Story> {
    const newStory = new this.storyModel(story)
    return newStory.save();
  }

  findAll(): Promise<Story[]> {
    return this.storyModel.find().exec();
  }

  findLikedAndPublic(): Promise<Story[]> {
    return this.storyModel.find({
      likes: { $gt: 20},
      privacy: 'public'
    }).exec();
  }

  findOneByTitle(title: string): Promise<Story> {
    return this.storyModel.findOne({title}).exec();
  }
}