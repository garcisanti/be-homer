import { CreateStoryInput } from '../../graphql.schema';

export class CreateStoryDto extends CreateStoryInput {
  launch_date: Date
  title: string
  privacy: string
  likes: number
}