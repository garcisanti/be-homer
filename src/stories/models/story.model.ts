import { Document } from 'mongoose';

export interface StoryModel extends Document {
    readonly launch_date: Date,
    readonly title: string,
    readonly privacy: string,
    readonly likes: number
  }