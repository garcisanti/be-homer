import * as mongoose from 'mongoose';

export const StorySchema = new mongoose.Schema({
    launch_date: Date,
    title: String,
    privacy: String,
    likes: Number
  });