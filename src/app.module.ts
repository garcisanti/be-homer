import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { StoriesModule } from './stories/stories.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    StoriesModule,
    MongooseModule.forRoot('mongodb://localhost/homer', {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }),
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
